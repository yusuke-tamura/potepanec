require 'rails_helper'

RSpec.describe "Categories", type: :request do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    it "テンプレートが表示されていること" do
      expect(response).to render_template :show
    end

    it "@taxonomiesがセットされていること" do
      expect(assigns(:taxonomies)).to match_array taxonomy
    end

    it "@taxonがセットされていること" do
      expect(assigns(:taxon)).to eq taxon
    end
  end
end
