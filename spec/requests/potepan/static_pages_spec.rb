require 'rails_helper'

RSpec.describe "StaticPages", type: :request do
  describe "#home" do
    let!(:new_products) { create_list(:product, 8, available_on: 2.days.ago) }
    let!(:latest_product) { create(:product, available_on: 1.days.ago) }

    before do
      get potepan_path
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    it "テンプレートが表示されていること" do
      expect(response).to render_template :home
    end

    it "新着商品が8つ表示されていること" do
      expect(controller.instance_variable_get("@new_products").length).to eq 8
    end

    it "新着商品の始めに、最新の商品が表示されているか" do
      expect(controller.instance_variable_get("@new_products").first).to eq latest_product
    end
  end
end
