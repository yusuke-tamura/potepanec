require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "#show" do
    let(:taxon) { create(:taxon) }
    let(:other_taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon], name: "RUBY ON RAILS TOTE") }
    let(:other_taxon_product) do
      create(:product, taxons: [other_taxon], name: "APACHE BASEBALL JERSEY")
    end
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_successful
    end

    it "テンプレートが表示されていること" do
      expect(response).to render_template :show
    end

    it "商品名が表示されていること" do
      expect(response.body).to include "RUBY ON RAILS TOTE"
    end

    it "関連商品が4つ表示されていること" do
      expect(controller.instance_variable_get("@related_products").length).to eq 4
    end

    it "異なるカテゴリの商品は表示されていないこと" do
      expect(response.body).not_to include "APACHE BASEBALL JERSEY"
    end
  end
end
