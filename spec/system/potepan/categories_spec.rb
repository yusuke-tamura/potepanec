require 'rails_helper'

RSpec.describe 'Products', type: :system do
  describe "カテゴリ詳細ページ" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let(:other_product) { create(:product) }

    before do
      visit potepan_category_path(taxon.id)
    end

    context "ページ全体について" do
      it "カテゴリタイトルが表示されていること" do
        expect(page).to have_content taxonomy.name
      end

      it "トップページへのリンクが機能していること" do
        click_link 'Home', match: :first
        expect(current_path).to eq potepan_path
      end
    end

    context "サイドバーについて" do
      it "カテゴリ詳細ページへのリンクが機能していること" do
        expect(page).to have_content taxon.name
        click_link taxon.name
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    context "商品一覧について" do
      it "カテゴリに紐づく商品のタイトル・価格が表示されていること" do
        expect(page).to have_content product.name
        expect(page).to have_content product.display_price
      end

      it "カテゴリに紐づかない商品は表示されていないこと" do
        expect(page).to have_no_content other_product.name
      end

      it "商品詳細ページへのリンクが機能していること" do
        click_link product.name
        expect(current_path).to eq potepan_product_path(product.id)
      end
    end
  end
end
