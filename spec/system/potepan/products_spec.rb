require 'rails_helper'

RSpec.describe 'Products', type: :system do
  describe "商品詳細ページ" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    context "商品詳細について" do
      it "商品が表示されていること" do
        within(".media-body") do
          expect(page).to have_content product.name
          expect(page).to have_content product.display_price
          expect(page).to have_content product.description
        end
      end

      it "トップページへのリンクが機能していること" do
        within(".pageHeader") do
          click_link 'Home'
          expect(current_path).to eq potepan_path
        end
      end
    end

    context "関連商品について" do
      it "関連商品が表示されていること" do
        within(".productsContent") do
          expect(page).to have_content related_product.name
          expect(page).to have_content related_product.display_price
        end
      end

      it "関連商品へのリンクが機能していること" do
        within(".productsContent") do
          expect(page).to have_content related_product.name
          expect(page).to have_content related_product.display_price
        end
        click_link related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end
  end
end
