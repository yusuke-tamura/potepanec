require 'rails_helper'

RSpec.describe 'StaticPages', type: :system do
  describe "トップページ" do
    let(:taxon) { create(:taxon) }
    let!(:new_products) { create_list(:product, 8, taxons: [taxon]) }

    before do
      visit potepan_path
    end

    context "新着商品" do
      it "新着商品が表示されていること" do
        expect(page).to have_content new_products.first.name
        expect(page).to have_content new_products.first.display_price
      end

      it "新着商品へのリンクが機能していること" do
        click_link new_products.first.name
        expect(current_path).to eq potepan_product_path(new_products.first.id)
      end
    end
  end
end
