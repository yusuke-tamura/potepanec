Spree::Product.class_eval do
  def related_products
    Spree::Product.in_taxons(taxons).distinct.where.not(id: id)
  end
end
