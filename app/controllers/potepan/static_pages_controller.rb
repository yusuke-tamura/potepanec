class Potepan::StaticPagesController < ApplicationController
  MAX_NEW_PRODUCTS = 8

  def home
    @new_products = Spree::Product.includes(master: [:default_price, :images]).
      order(available_on: "DESC").limit(MAX_NEW_PRODUCTS)
  end
end
